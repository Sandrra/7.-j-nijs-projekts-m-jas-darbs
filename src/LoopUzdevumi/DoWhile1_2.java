package LoopUzdevumi;

import java.util.Scanner;

public class DoWhile1_2 {
    public static void main (String[] args){
        int x,y;

        Scanner bucky = new Scanner(System.in);
        System.out.println("Input number amount that you want to print x: ");
        x = bucky.nextInt();
        System.out.print("Input starting number y: ");
        y = bucky.nextInt();
        int g = (y-x);
        do {
            System.out.println(y--);
        } while (y>=g);

    }
}


//Complete each of the following subtasks using all three loops - for, while, do-while.
//Note: Inside loop do NOT use ifs or any other control structures, besides
// loops themselves. For each of following tasks value represented as x, y and z
// should be entered by user.
//Think about: which loop is more suited for each of the tasks? Why?
//Tasks:
//
//    Print x integers starting from y descending