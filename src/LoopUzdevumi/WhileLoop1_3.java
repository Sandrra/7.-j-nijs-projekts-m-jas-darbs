package LoopUzdevumi;

import java.util.Scanner;
public class WhileLoop1_3 {
    public static void main (String[] args){
        int x,y,g;
        Scanner bucky = new Scanner(System.in);
        System.out.println("Input number count x:");
        x = bucky.nextInt();
        System.out.println("Input starting number , it must be odd number y:");
        y = bucky.nextInt();
        if (y % 2 == 0) {
            System.out.println("You didnt insert ODD number, please insert ONLY ODD number");
        }
        g = y;
        while ( g <(y+(x*2))){
            System.out.println(g);
            g+=2;
        }
    }
}

//Group 1 - Loops
//Complete each of the following subtasks using all three loops - for, while, do-while.
//Note: Inside loop do NOT use ifs or any other control structures,
// besides loops themselves. For each of following tasks value represented
// as x, y and z should be entered by user.
//Think about: which loop is more suited for each of the tasks? Why?
//Tasks:
//
//    Print x odd numbers starting from y Odd-nepāra