package LoopUzdevumi;

import java.util.Scanner;

public class DoWhile1task {
    public static void main (String[] args) {
        int x,y;
        Scanner bucky =  new Scanner(System.in);
        System.out.println("Input number amount x:");
        x = bucky.nextInt();
        System.out.println("Input starting number y: ");
        y = bucky.nextInt();
        int g = (y+x);
        do {
            System.out.println(y);
            y++;
        } while (y <= g);


    }
}


//Group 1 - Loops
//Complete each of the following subtasks using all three loops - for, while, do-while.
//Note: Inside loop do NOT use ifs or any other control structures, besides loops themselves. For each of
// following tasks value represented as x, y and z should be entered by user.
//Think about: which loop is more suited for each of the tasks? Why?
//Tasks:
//Print x integers starting from y ascending