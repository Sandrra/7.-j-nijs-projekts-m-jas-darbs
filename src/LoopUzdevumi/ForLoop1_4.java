package LoopUzdevumi;

import java.util.Scanner;

public class ForLoop1_4 {
    public static void main (String[] args){
        int x,y,g;
        Scanner bucky = new Scanner(System.in);
        System.out.println("Input number count x: ");
        x = bucky.nextInt();
        System.out.println("Please input starting number y note y must be even number:");
        y = bucky.nextInt();
        if (y% 2 !=0){
            System.out.println("You didnt insert EVEN  number!!!");
        }
        x = y+(x*2);
        for (g = y; g < x; g+=2){
            System.out.println(g);
        }
    }
}

//Group 1 - Loops
//Complete each of the following subtasks using all three loops - for, while, do-while.
//Note: Inside loop do NOT use ifs or any other control structures,
// besides loops themselves. For each of following tasks value represented
// as x, y and z should be entered by user.
//Think about: which loop is more suited for each of the tasks? Why?
//Tasks:
//
//  Print x even numbers starting from y