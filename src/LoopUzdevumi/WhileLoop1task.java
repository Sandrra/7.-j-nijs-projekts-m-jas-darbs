package LoopUzdevumi;

import java.util.Scanner;

public class WhileLoop1task {
    public static void main(String[] args) {
        Scanner bucky =  new Scanner(System.in);
        System.out.println("Input integer amount x: ");
        int x = bucky.nextInt();
        System.out.println("Input starting number y: ");
        int y = bucky.nextInt();
        int g = 0;
        while (g < x) {
            System.out.println("Print numbers starting from y ascending" + y++);
            g++;
        }

    }
}
//Group 1 - Loops
//Complete each of the following subtasks using all three loops - for, while, do-while.
//Note: Inside loop do NOT use ifs or any other control structures, besides loops themselves. For each of
// following tasks value represented as x, y and z should be entered by user.
//Think about: which loop is more suited for each of the tasks? Why?
//Tasks:
//Print x integers starting from y ascending